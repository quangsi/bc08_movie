import React from "react";
import { useSelector } from "react-redux";
import { userLocalStorage } from "../../api/localService";
import { NavLink } from "react-router-dom";

export default function Header() {
  let { info } = useSelector((state) => {
    return state.userReducer;
  });
  let handleLogout = () => {
    userLocalStorage.remove();
    window.location.reload();
    // window.location.href = "/";
  };
  let handleLogin = () => (window.location.href = "/login");
  let renderUserNav = () => {
    let classBtn = "border-2 border-black rounded px-7 py-3";
    if (info) {
      return (
        <>
          <span>{info.hoTen}</span>
          <button onClick={handleLogout} className={classBtn}>
            Đăng xuất
          </button>
        </>
      );
      // đã đăng nhập
    } else {
      return (
        <>
          <button onClick={handleLogin} className={classBtn}>
            Đăng nhập
          </button>
          <button className={classBtn}>Đăng kí</button>
        </>
      );
    }
  };

  return (
    <div className="h-20 flex items-center justify-between shadow-lg px-20">
      <NavLink to="/">
        <span className="text-3xl font-medium text-red-600 animate-pulse">
          CyberFlix
        </span>
      </NavLink>
      <div className="space-x-5">{renderUserNav()}</div>
    </div>
  );
}
