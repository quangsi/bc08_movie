import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getDetailMovie } from "../../api/api";
import { Progress } from "antd";

export default function DetailMovie() {
  // useParams => lấy id từ url
  let params = useParams();
  const [detail, setDetail] = useState({});

  useEffect(() => {
    // gọi api lấy chi tiết phim dựa vào id
    getDetailMovie(params.id)
      .then((res) => {
        console.log(res);
        setDetail(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div className=" container flex justify-between items-center">
      <img className="w-1/3 aspect-square" src={detail.hinhAnh} alt="" />
      <Progress
        size={400}
        format={(percent) => (
          <span className="text-red-600 font-medium  block">
            {percent / 10} Điểm
          </span>
        )}
        type="circle"
        strokeColor={"red"}
        strokeWidth={20}
        className="animate-spin"
        percent={detail.danhGia * 10}
      />
    </div>
  );
}

// Progress antd
