import React from "react";
import bgAnimate from "./animation_lmvj90wt.json";
import Lottie from "lottie-react";
export default function Banner() {
  return (
    <div className="w-1/2">
      <Lottie animationData={bgAnimate} loop={false} />
    </div>
  );
}
