import React, { useEffect, useState } from "react";
import { Carousel, ConfigProvider, message } from "antd";
import { getDataSlider } from "../../../api/api";
const contentStyle = {
  margin: 0,
  height: "160px",
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  background: "#364d79",
};
export default function Slider() {
  const [banner, setBanner] = useState([]);
  let fetchData = async () => {
    try {
      let response = await getDataSlider();
      console.log("😀 - fetchData - response", response);
      setBanner(response.data.content);
    } catch {
      message.error("đã có lỗi xảy ra");
    }
  };
  useEffect(() => {
    fetchData();
  }, []);
  // try catch
  const onChange = (currentSlide) => {
    console.log(currentSlide);
  };
  return (
    <ConfigProvider
      theme={{
        components: {
          Carousel: {
            dotHeight: 10,
            dotWidth: 60,
            dotActiveWidth: 100,
            /* here is your component tokens */
          },
        },
      }}
    >
      <Carousel autoplay effect="fade" afterChange={onChange}>
        {banner.map((item, index) => {
          return (
            <img
              src={item.hinhAnh}
              key={index}
              className=" h-40
              sm:h-64
              lg:h-96
              xl:h-200  w-full object-cover"
              alt=""
            />
          );
        })}
      </Carousel>
    </ConfigProvider>
  );
}

let user = {
  ["your_name"]: "alice Trần",
};
console.log("😀 - user", user["your_name"]);

// redux thunk - api
