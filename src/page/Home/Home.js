import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Header from "../../component/Header/Header";
import ListMovie from "./ListMovie/ListMovie";
import TabMovie from "./TabMovie/TabMovie";
import Slider from "./Slider/Slider";

export default function Home() {
  return (
    <div className="space-y-10">
      <Slider />
      <ListMovie></ListMovie>
      <TabMovie />
    </div>
  );
}
// hiện 1
// ko hiện 0
