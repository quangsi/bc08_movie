import React, { useEffect, useState } from "react";
import { getMovieByTheater } from "../../../api/api";
import { Tabs } from "antd";
import moment from "moment";
const onChange = (key) => {
  console.log(key);
};
export default function TabMovie() {
  const [danhSachHeThongRap, setDanhSachHeThongRap] = useState([]);
  useEffect(() => {
    getMovieByTheater()
      .then((res) => {
        setDanhSachHeThongRap(res.data.content);
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let renderDsPhim = (dsPhim) => {
    return dsPhim.map((phim) => {
      return (
        <div className="flex space-x-5 p-3 items-center">
          <img src={phim.hinhAnh} className="w-20 h-32 object-cover" alt="" />
          <div>
            <p>{phim.tenPhim}</p>
            <div className="grid grid-cols-4 gap-5">
              {phim.lstLichChieuTheoPhim.slice(0, 8).map((lichChieu) => {
                return (
                  <span className="bg-red-500 text-white rounded shadow px-5 py-2">
                    {moment(lichChieu).format("LL")}
                  </span>
                );
              })}
              {/* list lich chieu - moment js */}
            </div>
          </div>
        </div>
      );
    });
  };
  let handleHeThongRap = () => {
    return danhSachHeThongRap.map((heThongRap, index) => {
      return {
        key: index,
        label: <img className="w-16" src={heThongRap.logo} alt="" />,
        children: (
          <Tabs
            style={{
              height: 500,
            }}
            tabPosition="left"
            items={heThongRap.lstCumRap.map((cumRap) => {
              return {
                key: cumRap.tenCumRap,
                label: (
                  <div className="text-left w-96 whitespace-normal ">
                    <p className="text-green-800 font-medium ">
                      {cumRap.tenCumRap}
                    </p>
                    <p className="hover:text-green-800">{cumRap.diaChi}</p>
                  </div>
                ),
                children: (
                  <div
                    style={{
                      height: 500,
                      overflow: "scroll",
                    }}
                  >
                    {renderDsPhim(cumRap.danhSachPhim)}
                  </div>
                ),
              };
            })}
          />
        ),
      };
    });
  };
  // react responsive
  return (
    <div className="container  shadow  p-3 rounded border-2 border-l-black">
      <Tabs
        style={{
          height: 500,
        }}
        tabPosition="left"
        defaultActiveKey="1"
        items={handleHeThongRap()}
        onChange={onChange}
      />
    </div>
  );
}

// tab antd
// heThongRap, cumRap, phim, gioChieu
